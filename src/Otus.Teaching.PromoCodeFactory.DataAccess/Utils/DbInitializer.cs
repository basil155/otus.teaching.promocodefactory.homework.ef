﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Utils;

public class DbInitializer(ApplicationDbContext context) : IDbInitializer
{
    public void DbReCreate()
    {
        context.Database.EnsureDeleted();
        context.Database.EnsureCreated();
        SeedFakeData();
    }

    public void DbMigrate()
    {
        var firstRun = !context.Database.CanConnect();

        if (context.Database.GetPendingMigrations().Any())
        {
            context.Database.Migrate();
        }

        if (firstRun)
        {
            SeedFakeData();
        }
    }

    public void SeedFakeData()
    {
        context.AddRange(FakeDataFactory.Roles);

        context.AddRange(FakeDataFactory.Employees.Select(e =>
            {
                e.Role = context.Roles.Find(e.Role.Id);
                return e;
            }
        ));

        context.AddRange(FakeDataFactory.Preferences);

        context.AddRange(FakeDataFactory.Customers.Select(c =>
            {
                c.Preferences = c.Preferences.Select(p =>
                    context.Preferences.Find(p.Id)
                ).ToList();
                return c;
            }
        ));
        context.SaveChanges();
    }

}