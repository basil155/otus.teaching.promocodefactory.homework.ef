﻿using Otus.Teaching.PromoCodeFactory.Services.Contracts;

namespace Otus.Teaching.PromoCodeFactory.Services.Abstractions;

public interface IPreferenceService
{
    Task<List<PreferenceResponse>> GetAsync();
}