using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class EfRepository<T> : IRepository<T> where T : BaseEntity
{
    protected readonly ApplicationDbContext Context;
    private readonly DbSet<T> _entitySet;

    public EfRepository(ApplicationDbContext context)
    {
        Context = context;
        _entitySet = Context.Set<T>();
    }

    public IQueryable<T> GetAll()
    {
        return _entitySet;
    }

    public async Task<IEnumerable<T>> GetAllAsync()
    {
        return await _entitySet.ToListAsync();
    }

    public async Task<T> GetByIdAsync(Guid id)
    {
        return await _entitySet.FindAsync(id);
    }

    public async Task<Guid> AddAsync(T entity)
    {
        return (await _entitySet.AddAsync(entity)).Entity.Id;
    }

    public void Update(T entity)
    {
        _entitySet.Update(entity);
    }

    public void Delete(T entity)
    {
        _entitySet.Remove(entity);
    }
}