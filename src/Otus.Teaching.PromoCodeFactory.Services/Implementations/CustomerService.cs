﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.UnitOfWork;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;

namespace Otus.Teaching.PromoCodeFactory.Services.Implementations;

public class CustomerService(IUnitOfWork unitOfWork, IMapper mapper) : ICustomerService
{
    public async Task<List<CustomerShortResponse>> GetAsync()
    {
        var result = await unitOfWork.CustomerRepository.GetAllAsync();
        return mapper.Map<List<CustomerShortResponse>>(result);
    }

    public async Task<CustomerResponse?> GetByIdAsync(Guid id)
    {
        var result = await unitOfWork.CustomerRepository
            .GetAll()
            .Include(c => c.PromoCodes)
            .Include(c => c.Preferences)
            .FirstOrDefaultAsync(c => c.Id == id);

        return mapper.Map<CustomerResponse>(result);
    }

    public async Task<Guid> CreateAsync(CreateOrEditCustomerRequest request)
    {
        var customer = mapper.Map<Customer>(request);
        customer.Preferences = await unitOfWork.PreferenceRepository.GetAll()
            .Where(p => request.PreferenceIds.Contains(p.Id))
            .ToListAsync();

        var id = await unitOfWork.CustomerRepository.AddAsync(customer);
        await unitOfWork.SaveChangesAsync();

        return id;
    }

    public async Task<bool> UpdateByIdAsync(Guid id, CreateOrEditCustomerRequest request)
    {
        var customer = await unitOfWork.CustomerRepository
            .GetAll()
            .Include(c => c.PromoCodes)
            .Include(c => c.Preferences)
            .FirstOrDefaultAsync(c => c.Id == id);

        if (customer == null)
        {
            return false;
        }

        customer.FirstName = request.FirstName;
        customer.LastName = request.LastName;
        customer.Email = request.Email;
        customer.Preferences = await unitOfWork.PreferenceRepository.GetAll()
            .Where(p => request.PreferenceIds.Contains(p.Id))
            .ToListAsync();

        unitOfWork.CustomerRepository.Update(customer);
        await unitOfWork.SaveChangesAsync();

        return true;
    }

    public async Task<bool> DeleteByIdAsync(Guid id)
    {
        var customer = await unitOfWork.CustomerRepository
            .GetAll()
            .Include(c => c.PromoCodes)
            .Include(c => c.Preferences)
            .FirstOrDefaultAsync(c => c.Id == id);

        if (customer == null)
        {
            return false;
        }

        unitOfWork.CustomerRepository.Delete(customer);
        await unitOfWork.SaveChangesAsync();

        return true;
    }
}