﻿using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.UnitOfWork;

public interface IUnitOfWork
{
    IRepository<Employee> EmployeeRepository { get; }
    IRepository<Role> RoleRepository { get; }
    IRepository<Customer> CustomerRepository { get; }
    IRepository<Preference> PreferenceRepository { get; }
    IRepository<PromoCode> PromoCodeRepository { get; }

    Task SaveChangesAsync();
    
}