﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.UnitOfWork;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess.UoW;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Utils;

public static class ApplicationDatabaseHelper
{
    public static IServiceCollection AddApplicationDatabase(this IServiceCollection serviceCollection,
        IConfiguration configuration)
    {
        var connectionString = configuration.GetValue<string>("ConnectionStrings:SQLite");

        serviceCollection.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlite(connectionString,
                    o => o.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery))
            //.EnableSensitiveDataLogging()
        );

        serviceCollection.AddScoped<IRepository<Employee>, EfRepository<Employee>>()
            .AddScoped<IRepository<Role>, EfRepository<Role>>()
            .AddScoped<IRepository<Customer>, EfRepository<Customer>>()
            .AddScoped<IRepository<Preference>, EfRepository<Preference>>()
            .AddScoped<IRepository<PromoCode>, EfRepository<PromoCode>>();

        serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

        serviceCollection.AddScoped<IDbInitializer, DbInitializer>();

        return serviceCollection;
    }
}