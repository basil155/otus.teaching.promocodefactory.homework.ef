﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController(ICustomerService customerService) : ControllerBase
    {
        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Успешное выполнение</response>
        [HttpGet]
        [ProducesResponseType(typeof(List<CustomerShortResponse>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var result = await customerService.GetAsync();

            return Ok(result);
        }

        /// <summary>
        /// Получить данные клиента по Id
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Успешное выполнение</response>
        /// <response code="204">Нет данных</response>
        [HttpGet("{id:guid}")]
        [ProducesResponseType(typeof(CustomerResponse), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.NoContent)]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var result = await customerService.GetByIdAsync(id);

            return result != null ? Ok(result) : NoContent();
        }

        /// <summary>
        /// Добавить данные клиента
        /// </summary>
        /// <returns></returns>
        /// <response code="201">Успешное выполнение</response>
        /// <response header="Location">Расположение объекта</response>
        [HttpPost]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var id = await customerService.CreateAsync(request);

            return Created($"api/v1/Employees/{id}", null);
        }

        /// <summary>
        /// Изменить данные клиента по Id
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Успешное выполнение</response>
        /// <response code="400">Объект не найден</response>
        [HttpPut("{id:guid}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var success = await customerService.UpdateByIdAsync(id, request);

            return success ? Ok() : BadRequest();
        }

        /// <summary>
        /// Удалить данные клиента по Id
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Успешное выполнение</response>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            await customerService.DeleteByIdAsync(id);

            return Ok();
        }
    }
}