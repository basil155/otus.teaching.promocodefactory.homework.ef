﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.UnitOfWork;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;

namespace Otus.Teaching.PromoCodeFactory.Services.Implementations;

public class PreferenceService(IUnitOfWork unitOfWork, IMapper mapper) : IPreferenceService
{
    public async Task<List<PreferenceResponse>> GetAsync()
    {
        var result = await unitOfWork.PreferenceRepository.GetAllAsync();
        return mapper.Map<List<PreferenceResponse>>(result);
    }

}