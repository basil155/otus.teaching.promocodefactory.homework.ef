﻿namespace Otus.Teaching.PromoCodeFactory.Services.Contracts;

public class PreferenceResponse
{
    public Guid Id { get; set; }
    public string Name { get; set; }
}